import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Scanner;


public class AlphabetSoup {
	static char [][] grid = {};
	static int row, col;
	//LinkedHashMap to maintain insertion order
	static LinkedHashMap<String, ArrayList<Integer>> key = new LinkedHashMap<String, ArrayList<Integer>>();
	
	static enum direction { UP, DOWN, RIGHT, LEFT, UPRIGHT, UPLEFT, DOWNRIGHT, DOWNLEFT };
	
	/*This is the main method that reads from the file specified by the user. 
	 * After text processing and manipulation, the word search puzzle is solved*/
	public static void main(String args[]) throws IOException {
		//Getting file name from user
		Scanner scanName = new Scanner(System.in);
		System.out.println("Enter ASCII text file name: ");
		String fileName = scanName.nextLine();
		
		scanName.close();
		
		//Parse File Data
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			
			//Reading line by line
			String line = null;
			StringBuilder sb = new StringBuilder();
			int position = 0;
			
			while( (line = br.readLine()) != null ) {
				sb.append(line).append("\n");
				
				if(position == 0) { //Grid dimensions from first line
					getDimensions(line);
					
				} else if( position < row + 1) { //Actual chars in grid
					gridCpy(line, position -1); // -1 due to first line in file
					
				} else { //Words to be found within grid
					addToKey(line);
				}
				position++;
			}
			br.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		/*Solving the word search problem*/
		solve(grid);
		/*Printing locations of found words*/
		printResult();
	}
	
	/*This method sets the size of the grid by extracting values from a file's
	 *first line. */
	public static void getDimensions(String line) throws IOException {
		String[] dimensions;
		
		dimensions = line.replaceAll("\\s+", "").split("x");
		row = Integer.valueOf(dimensions[0]);
		col = Integer.valueOf(dimensions[1]);
		
		if(row != col || row <= 0 || col <= 0) {
			throw new IOException("Illegal Grid Size!");
		}
		grid = new char [row][col];	
	}
	
	/*This method copies the formatted grid input into the grid row by row.*/
	public static void gridCpy(String line, int currPos) {
		char[] letters = line.replaceAll(" ", "").toUpperCase().toCharArray();
		grid[currPos] = letters;
	}
	
	/*This method adds the words to find within the grid to the map. */
	public static void addToKey(String line) {
		String lineChange = line.replaceAll("\\s+", "").toUpperCase();

		/*NO duplicates && Handling of new lines at EOF*/
		if(!key.containsKey(lineChange) && line.length() > 0) {
			key.put(lineChange, null);
		}
	}
	
	/*This method solves the word search problem by iterating through each of 
	 * the list of words to be found. For each word, every cell is checked to see
	 * if there is a match for the word's first letter. If there is a match, then
	 * every direction is tested to see if the full word exists. */
	public static void solve(char[][] grid) throws IOException {
		for(String word: key.keySet()) {
			String currWord = word;
			char beginChar = currWord.charAt(0);
			/*Loop through grid */
			for(int i = 0; i < grid.length; i++) {
				for(int j = 0; j < grid[i].length; j++) {
					/*cell matches first letter of word*/
					if(grid[i][j] == beginChar) {
						/*Test each direction.*/
						directionTest(i, j, currWord, direction.UP);
						directionTest(i, j, currWord, direction.DOWN);
						directionTest(i, j, currWord, direction.RIGHT);
						directionTest(i, j, currWord, direction.LEFT);
						directionTest(i, j, currWord, direction.UPRIGHT);
						directionTest(i, j, currWord, direction.UPLEFT);
						directionTest(i, j, currWord, direction.DOWNRIGHT);
						directionTest(i, j, currWord, direction.DOWNLEFT);
						
					}
				}
			}
		}
	}
	
	/*This method tests to see if the full word exists from the passed in parameter
	 * starting point (i, j) with the passed in direction (dir). */
	public static void directionTest(int i, int j, String word, direction dir ) throws IOException {
		if( outOfBounds(i, j, word, dir) ) {
			return;
		}
		int rowI = i, colJ = j, wordLen = word.length();
		
		if (dir == direction.UP) {
			/*Check if each letter in direction matches each letter in word*/
			for(int charIndex = 0; charIndex < wordLen; charIndex++) {
				if(grid[rowI][colJ] != word.charAt(charIndex)) {
					return;
				} else {
					//Move index(s) up in grid
					rowI--;
				}
			}
			/*Add solution(s) if all chars in direction match all chars in word*/
			if(key.get(word) == null) {
				/*First time word is found*/
				key.put(word, new ArrayList<Integer>(Arrays.asList(i, j, rowI + 1, colJ)));
			} else {
				/*Another word is found*/
				key.get(word).add(i);
				key.get(word).add(j);
				key.get(word).add(rowI + 1 );
				key.get(word).add(colJ);
			}
			
		} else if (dir == direction.DOWN) {
			/*Check if each letter in direction matches each letter in word*/
			for(int charIndex = 0; charIndex < wordLen; charIndex++) {
				if(grid[rowI][colJ] != word.charAt(charIndex)) {
					return;
				} else {
					//Move index(s) down in grid
					rowI++;
				}
			}
			/*Add solution(s) if all chars in direction match all chars in word*/
			if(key.get(word) == null) {
				/*First time word is found*/
				key.put(word, new ArrayList<Integer>(Arrays.asList(i, j, rowI - 1 , colJ)));
			} else {
				/*Another word is found*/
				key.get(word).add(i);
				key.get(word).add(j);
				key.get(word).add(rowI - 1);
				key.get(word).add(colJ);
			}
			
		} else if (dir == direction.RIGHT) {
			/*Check if each letter in direction matches each letter in word*/
			for(int charIndex = 0; charIndex < wordLen; charIndex++) {
				if(grid[rowI][colJ] != word.charAt(charIndex)) {
					return;
				} else {
					//Move index(s) right in grid
					colJ++;
				}
			}
			/*Add solution(s) if all chars in direction match all chars in word*/
			if(key.get(word) == null) {
				/*First time word is found*/
				key.put(word, new ArrayList<Integer>(Arrays.asList(i, j, rowI, colJ - 1)));
			} else {
				/*Another word is found*/
				key.get(word).add(i);
				key.get(word).add(j);
				key.get(word).add(rowI);
				key.get(word).add(colJ - 1);
			}
			
		} else if (dir == direction.LEFT) {
			/*Check if each letter in direction matches each letter in word*/
			for(int charIndex = 0; charIndex < wordLen; charIndex++) {
				if(grid[rowI][colJ] != word.charAt(charIndex)) {
					return;
				} else {
					//Move index(s) left in grid
					colJ--;
				}
			}
			/*Add solution(s) if all chars in direction match all chars in word*/
			if(key.get(word) == null) {
				/*First time word is found*/
				key.put(word, new ArrayList<Integer>(Arrays.asList(i, j, rowI, colJ + 1)));
			} else {
				/*Another word is found*/
				key.get(word).add(i);
				key.get(word).add(j);
				key.get(word).add(rowI);
				key.get(word).add(colJ + 1);
			}
			
		} else if (dir == direction.UPRIGHT) {
			/*Check if each letter in direction matches each letter in word*/
			for(int charIndex = 0; charIndex < wordLen; charIndex++) {
				if(grid[rowI][colJ] != word.charAt(charIndex)) {
					return;
				} else {
					//Move index(s) up and right in grid
					rowI--;
					colJ++;
				}
			}
			/*Add solution(s) if all chars in direction match all chars in word*/
			if(key.get(word) == null) {
				/*First time word is found*/
				key.put(word, new ArrayList<Integer>(Arrays.asList(i, j, rowI + 1, colJ - 1)));
			} else {
				/*Another word is found*/
				key.get(word).add(i);
				key.get(word).add(j);
				key.get(word).add(rowI + 1);
				key.get(word).add(colJ - 1);
			}
			
		} else if (dir == direction.UPLEFT) {
			/*Check if each letter in direction matches each letter in word*/
			for(int charIndex = 0; charIndex < wordLen; charIndex++) {
				if(grid[rowI][colJ] != word.charAt(charIndex)) {
					return;
				} else {
					//Move index(s) up and left in grid
					rowI--;
					colJ--;
				}
			}
			/*Add solution(s) if all chars in direction match all chars in word*/
			if(key.get(word) == null) {
				/*First time word is found*/
				key.put(word, new ArrayList<Integer>(Arrays.asList(i, j, rowI + 1, colJ + 1)));
			} else {
				/*Another word is found*/
				key.get(word).add(i);
				key.get(word).add(j);
				key.get(word).add(rowI + 1);
				key.get(word).add(colJ + 1);
			}
			
		} else if (dir == direction.DOWNRIGHT) {
			/*Check if each letter in direction matches each letter in word*/
			for(int charIndex = 0; charIndex < wordLen; charIndex++) {
				if(grid[rowI][colJ] != word.charAt(charIndex)) {
					return;
				} else {
					//Move index(s) down and right in grid
					rowI++;
					colJ++;
				}
			}
			/*Add solution(s) if all chars in direction match all chars in word*/
			if(key.get(word) == null) {
				/*First time word is found*/
				key.put(word, new ArrayList<Integer>(Arrays.asList(i, j, rowI - 1, colJ - 1)));
			} else {
				/*Another word is found*/
				key.get(word).add(i);
				key.get(word).add(j);
				key.get(word).add(rowI - 1 );
				key.get(word).add(colJ - 1);
			}
			
		} else if (dir == direction.DOWNLEFT) {
			/*Check if each letter in direction matches each letter in word*/
			for(int charIndex = 0; charIndex < wordLen; charIndex++) {
				if(grid[rowI][colJ] != word.charAt(charIndex)) {
					return;
				} else {
					//Move index(s) down and left in grid
					rowI++;
					colJ--;
				}
			}
			/*Add solution*/
			if(key.get(word) == null) {
				/*First time word is found*/
				key.put(word, new ArrayList<Integer>(Arrays.asList(i, j, rowI - 1, colJ + 1)));
			} else {
				/*Another word is found*/
				key.get(word).add(i);
				key.get(word).add(j);
				key.get(word).add(rowI - 1);
				key.get(word).add(colJ + 1);
			}
		}  else { throw new IOException("Illegal Direction!"); }
		
	}
	
	/*This method checks if it is reasonable to search for a word in a specified
	 * direction. Limits indexing out of bounds. Returns true if checking the 
	 * direction will result in errors. Returns false if checking the direction 
	 * is okay. */
	public static boolean outOfBounds(int i, int j, String word, direction dir) {
		boolean val = false;
		if( dir == direction.UP && (i + 1) - word.length() < 0 ) {
			val = true;
		}
		if( dir == direction.DOWN && i + word.length() > grid[i].length ) {
			val = true;
		}
		if( dir == direction.RIGHT && j + word.length() > grid.length ) {
			val = true;
		}
		if( dir == direction.LEFT && (j + 1) - word.length() < 0 ) {
			val = true;
		}
		if( dir == direction.UPRIGHT && (j + word.length() > grid.length 
				|| (i + 1) - word.length() < 0) ) {
			val = true;
		}
		if( dir == direction.UPLEFT && ((j + 1) - word.length() < 0 
				|| (i + 1) - word.length() < 0) ) {
			val = true;
		}
		if( dir == direction.DOWNRIGHT && (j + word.length() > grid.length 
				|| i + word.length() > grid[i].length) ) {
			val = true;
		}
		if( dir == direction.DOWNLEFT && ((j + 1) - word.length() < 0 
									||  i + word.length() > grid[i].length) ) {
			val = true;
		}
		return val;
	}
	
	/*This method prints the words found in the grid and at which location(s)*/
	public static void printResult() {
		for(String word: key.keySet()) {
			for(int i = 0; i < (key.get(word).size()) ; i += 4) {
				if( i == 0) {
					System.out.print( word + " " + key.get(word).get(0 + i ) + ":" 
							+ key.get(word).get(1 + i ) + " " + key.get(word).get(2 + i ) 
										+ ":" + key.get(word).get(3 + i ));
				} else {
					System.out.print(" " + key.get(word).get(0 + i ) + ":" 
							+ key.get(word).get(1 + i ) + " " + key.get(word).get(2 + i ) 
										+ ":" + key.get(word).get(3 + i ));
				}
			}
			System.out.println();
		}
	}
}